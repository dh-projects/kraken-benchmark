# KRAKEN-BENCHMARK, <i>Fluid and Experimental evaluation dashboard for HTR/OCR projects</i>

<img src="./static/images/KB_logo.png" alt="KB-logo" width="120px">
<img src="./static/images/inria_logo_grisbleu.png" alt="KB-logo" width="190px">

Kraken-Benchmark allows you to create a metrics dashboard and visualizer to evaluate your machine learning transcription and segmentation models. 

## Description
Kraken-Benchmark is Python application partially based on Flask which evaluates the performances of a (Kraken) 
transcription model given examples of perfect transcriptions. The various metrics generated by Kraken-Benchmark 
are displayed in a web application in the form of a dashboard. 

<!--Kraken-Benchmark also relies on the [Kraken API](http://kraken.re) (PSL-eScripta) to perform the transcription of your images.-->

## Metrics
Most of metrics use classic syntactic similarity algorithms and fuzzy string matching research; among these:
- Ratcliff / Obershelp algorithm
- Levenshtein & Levenshtein distance algorithm
- Word Error Rate, Character Error Rate, Word Accuracy
- Hamming distance

There are also displayed in the form of graphs: 
- histogram of the operations of passing from a reference sentence to a predicted sentence
- confusion matrix of the most frequent pairs of character errors
- ranking of the most frequent pairs of character errors
- visualizations of sequences in the form of signals

Additionally, Kraken-Benchmark allows you to refine the parameters of your models with the interpretation 
of your results, to design an evaluation pipeline from the transcription of your text corpus to more ambitious 
NLP projects such as entity recognition named, segmentation, disambiguation, word-spotting etc.

This is why the application already integrates metrics such as:
- jaccard index
- cosine similarity (using TF-IDF vectors, bag of words metric)

<!-- This aspect should evolve with the integration of the [entity-fishing](http://cloud.science-miner.com/nerd/) API. !-->

<!--
## Stack

...complete or modify this section in progress...

Env : Anaconda

HTR/OCR system : Kraken API

Pre-processing text data : NLTK

Data Visualizations : Matplotlib, Seaborn, Pandas, Difflib

Metrics : STS Tools lib, Sklearn, Numpy, python-Levenshtein, Kraken API, Difflib

Web application : Flask

Research : Jupyter notebook

!-->

## Install

First, clone kraken-Benchmark repository:

```$ git clone https://gitlab.inria.fr/dh-projects/kraken-benchmark.git```

```$ cd kraken-benchmark/```

create a new conda environment and activate it:

```$ conda env create --file environment.yml```

```conda activate kraken-benchmark```


## Process

<img src="./static/images/KB_flow.png" alt="KB-flow" width="700px">

Your input should be store as a directory containing 2 directories: `data/` (for images and ground 
truth); and `models` (for models). Only `.mlmodel` files will be accepted in `models/`.

```
├── {input}/
│    ├── data/   : contains reference transcription and images 
│    ├── models/ : contains kraken transcription models  

```

Kraken-benchmark automatically detects if your XML files follow the PAGE standard. Eventually, it will be possible to 
load ALTO XML files as well. 

Note that when you provide ground truth in the form of TXT files, that evaluation of the model will be
biased by the result of the segmentation task. It is therefor preferable to use the Page XML format for the
ground truth. 

To run a test of the application, you can use data stored in `data_test/` (default location of input).

<!--
2. Rename your files, with labels, to match your ground truth and images, such as :
- filename_image_1.jpeg, filename_image_2.jpeg, filename_image_3.jpeg etc.
- filename_GT_1.txt, filename_GT_2.txt, filename_GT_3.txt etc.
!-->

## Usages 

<!--...complete or modify this section in progress...-->

In `kraken-benchmark/` enter the following command:

- if you're using txt files as ground truth:
```$ python run.py``` 
> equivalent to `python run.py -i ./data_test/ -f txt`

- if you're using (page) xml files as ground truth:
```python run.py -f xml```


## Options

<!--...complete or modify this section in progress...-->

1. [-i, --input] Specify location of images, models and ground truth if
not in default location (default = ./data_test/)

2. [-f, --format] Specify the format of the provided ground truth (xml or txt)

<!-- 2. [label] Attach a metadata description on your each set's
image to display it in the HTML dashboard -->

3. [-v, --verbosity] View all the objects processed during the automatic transcription phase

<!--4. [clean-text] Performs a few cleanning steps to remove
non-alphabetic characters, punctuation, numbers.
depends on the needs of the project. (in construction...)-->

