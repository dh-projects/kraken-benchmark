#!/bin/sh

# requirements sur slave
#(sudo) apt-install python-pip
#(sudo) apt install virtualenv

echo "### ----- BUILDING ENVIRONMENT ----- ###"
virtualenv venv -p python3
. venv/bin/activate  #"source venv/bin/activate" #fails
python --version
echo ">> getting requirements"
pip install -r requirements.txt

echo "### ----- LINTER ----- ###"
# this test will need to be better applied further in the developpement
# we take every Python file and apply pylinter to it

for f in $(find ./kraken-benchmark/ -name '*.py'); do pylint-fail-under --fail_under 7.0 $f; done


echo "### ----- TEST ----- ###"
echo "Hey, it would be cool to test if the script actually runs..."