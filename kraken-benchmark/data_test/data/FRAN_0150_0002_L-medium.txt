~~~
J'occupais depuisle mois de Novembre I9II la Préfecture
du Nord quand, le I4 Janvier I9I3, M. POINCARE, renonçant à sa
tactique de prudente réserve, se décida à prendre le pouvoir.
Se dérober encore dans les circonstances troubles qui avaient
provoqué la chute du cabinet Caillaux, c'eut été compromettre
la brillante ascension de sa carrière. Les débuts de son Mini-
stère étaient pour satisfaire ses plus hautes ambitions. Son
action gouvernementale bénéficiait du prestige que lui valaient
l’universalité de ses connaissances et une réputation de probité
dont tous nos personnages consulaires n’auraient pu se prévaloir
Il avait réduit à l’impuissance l'opposition d’adversaires qui
prétendaient que le caractère n'était pas chez lui à la hauteur
du talent et que sa fermeté que l'on vantait était purement ver-
bale. Mais à la vérité, ces critiques de parlementaires ne trou-
vaient qu’un faible écho dans l’opinion. Le vigilance du patri-
otisme, dont le représentant le plus qualifié de le Lorraine
avait donné des preuves en maintes circonstances, rassurait le
pays qui depuis l’affaire de la Légion Etrangère, suivie de la
crise d’~~~gadir, aspirait à voir à la tête du gouvernement un
chef capable de prévoir et de prévenir tout péril extérieur.
En Juillet I9I3 M. Poincaré semblait à l"apogée de sa
fortune politique et, c’est fort de la confiance dont il était
entouré, qu’il résolut de se rendre en Russie pour resserrer les
liens qui nous unissaient à notre alliée. Il réalisa son dessein
