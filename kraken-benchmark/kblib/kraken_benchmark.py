#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""KRAKEN-BENCHMARK CLI PROGRAM

author: Lucas Terriel
contributors / reviewers : Alix Chagué
date : 22/07/2020

-| Summary |-
=============

Main script to launch Kraken-Benchmark.
This script launches a tool in the form of CLI to perform
the transcription of images from the ocr kraken system.
This script also creates objects belonging to STS Tools,
a library to calculate the performance of the transcription model.
Finally, it refers to the function responsible for generating the HTML metrics
dashboard in the form of a Flask application.

-| Usage |-
===========

kraken_benchmark.py [-h] [--input INPUT] [--label] [--verbosity] [--clean_text]

-| Options |-
=============

1. [:input:] Specify location of images, models and ground truth if
not in default location (default = current directory)

2. [:label:] Attach a metadata description on your each set's
image to display it in the HTML dashboard

3. [:verbosity:] View all the objects processed during the automatic transcription phase

4. [:clean-text:] Performs a few cleanning steps to remove
non-alphabetic characters, punctuation, numbers.
depends on the needs of the project.

-| Basic I/O |-

Input => Images / Ground Truth Transcription (.txt files) / model (.mlmodel).
output => kraken benchmark metrics report (.html) with Flask.

# TODO(Lucas) : slice get_transcription more finely to optimize
# TODO(Lucas) : make binarization optional
"""

# built-in packages
import os
import sys
import time
import unicodedata

# external packages
from kraken import rpred, pageseg, binarization
import pyfiglet
from tqdm import tqdm

# local packages
from .kb_utils.kb_utils import (get_list_tuple, get_metadata, report_log, try_control_step,
                                binarize_images, segment_images, predict_transcription, is_binary_model)
from .kb_utils.kb_io import (collect_input, parse_ground_truth_files, load_images, load_model)
from .STS_Tools.SynSemTS import TranscriptionMetricsTools
from .kb_report.routing import generate_html_report

DEFAULT_FORMAT = "txt"
ACCEPTED_FORMATS = ["txt", "xml"]


def get_transcription_txt(images_loaded: list, model_loaded: str, verbosity: bool) -> list:
    """built from images and an offline recognition model of the text
        of the separate transcripts for each image.
        Process
        -------
        * 2- Binarization
        * 3- Segmentation
        * 4- Text recognition
        * 5- Convert transcription kraken object in string format

        Args:
            images_loaded (list): list of user's images (ImgParser objects)
            model_loaded (<object>): loaded kraken model
            verbosity (bool): if user activate verbose option

        Returns:
            list: list contains the text prediction
        """
    #images_binarized = binarize_images(images_loaded, verbosity) #<- this step is done when parsing the image with ImgParser
    images_segmented = segment_images(images_loaded, verbosity)
    # create binarized image-segmented image pairs in order to create text recognition
    pairs_image_segments = get_list_tuple(images_loaded, images_segmented)
    predictions = predict_transcription(pairs_image_segments, model_loaded, verbosity)

    transcriptions = []
    for prediction in tqdm(predictions, desc="Transcription in progress :"):
        # create a canvas to accommodate lines of text
        # prediction in string format contained in an image
        canvas = ""
        try:
            for line in prediction:
                # .prediciton is a kraken_ocr_record class attribute for recover the text in
                # kraken.rpred.mm_rpred object
                canvas += f"{unicodedata.normalize('NFC', line.prediction)}"
            transcriptions.append(canvas)
        except Exception as exception:
            report_log(f"Error : unable to transcribe - {prediction}", "E")
            report_log(f"type : {exception}")
            sys.exit('program exit')

    try_control_step(transcriptions, predictions, "Transcription")
    return transcriptions


def get_transcription_xml(images_loaded, model_loaded, ground_truth_files, verbosity) -> list:
    """Generate a transcription from a series of images and coordinates
    Parameters
    ----------
    images_loaded (list): images loaded with PIL
    model_loaded (object): mlmodel loaded with kraken
    ground_truth_files (list): list of PagexmlParser
    verbosity (bool): (unused)

    Returns (list): transcriptions (a list of str)
    -------

    """
    transcriptions = []
    pairs_images_file = get_list_tuple(images_loaded, ground_truth_files)

    for im, file in tqdm(pairs_images_file, desc='Transcription in progress :'):
        if is_binary_model(model_loaded):
            image = im.binarized
            report_log(f"Transcribing {im.filename.split(os.sep)[-1]} after binarization." +
                       "This may impact the quality of the transcription.", "W")
        else:
            image = im.loaded_image
        transcription = ""
        for bound in file.bounds:
            it = rpred.rpred(
                model_loaded, image,
                bounds=bound,
                pad=16,
                bidi_reordering=True)
            transcription += "".join([t.prediction for t in list(it)]) + "\n"
        transcriptions.append(transcription)
    # run the model and get a prediction -> https://github.com/mittagessen/kraken/blob/master/kraken/rpred.py#L353
    # return a list of transcription (can we have the same type of objects as
    # get_transcriptions_txt()
    try_control_step(transcriptions, images_loaded, "Transcription")
    return transcriptions


def get_transcription(images: list, model: str, verbosity: bool, mode: str, ground_truth=None) -> list:
    """Generate a list of string (transcriptions) based  on images a model and sometimes XML files
    Parameters
    ----------
    images (list): list of path to image files
    model_loaded (str): path to mlmodel file
    verbosity (bool): for verbosity
    mode (str): xml | txt - will determine the transcription scenario
    ground_truth_files (None|list): list of PagexmlParser objects if mode == "xml"

    Returns (list): transcriptions (a list of str)
    -------
    """
    model_loaded = load_model(model)
    # STEP 2 : Loading images
    images_loaded = load_images(images, verbosity)
    if mode == "txt":
        list_transcriptions = get_transcription_txt(images_loaded, model_loaded, verbosity)
    elif mode == "xml":
        if ground_truth:
            list_transcriptions = get_transcription_xml(images_loaded, model_loaded, ground_truth, verbosity)
        else:
            report_log("No ground truth...", "E")
            list_transcriptions = None
    else:
        report_log("Not a valid mode to get transcription", "E")
        list_transcriptions = None
    return list_transcriptions


def kraken_benchmark(path_to_input, verbosity, label, clean_text, input_format) -> None:
    """Start Kraken Benchmark!
    Activate the Flask application at the end of the process.

    Parameters
    ----------
    path_to_input : Path to input (can be relative)
    verbosity : Print a series of execution messages
    label : Attach a description to user's images
    clean_text : Performs few clean steps on text

    Returns
    -------
    """

    # ***************************
    # ****** INITIALISATION *****
    # ******    SCRIPT      *****
    # ***************************

    # ----- ASCII edition logo
    ascii_logo_kraken_benchmark = pyfiglet.figlet_format("Kraken Benchmark", font="digital")

    # ----- Introducing program : 5, 4, 3, 2, 1...take off !
    report_log(f"        \u03A9 WELCOME TO \u03A9 ")
    report_log(ascii_logo_kraken_benchmark)
    if label:
        report_log('* Label mode activate *')
    if clean_text:
        report_log('* Clean text mode activate *')
    time.sleep(5)

    # ---- Retreive input
    if input_format not in ACCEPTED_FORMATS:
        input_format = DEFAULT_FORMAT
    model_name, model, images, group_gt = collect_input(path_to_input, input_format)

    if label:
        metadata = get_metadata(images)
    else:
        metadata = None

    # Build a list contains open & read IO.Wrapper files
    gt_set = parse_ground_truth_files(group_gt, input_format)

    # ---- RUN 1 : OCR sequence start
    transcriptions = get_transcription(images, model, verbosity, input_format, gt_set)

    # ---- RUN 2 : Grouping ground truth transcription, prediction, and image
    group_gt_model_list = get_list_tuple(gt_set, transcriptions, images)  # TODO refactor

    # ---- RUN 3 : Metrics Object creation sequence start
    list_statistics = []

    for ground_truth_source, prediction, image in tqdm(group_gt_model_list,
                                                       desc='metrics objects are being created...'):
        # creates objects which allow to give the different
        # metrics for the evaluation of the transcription
        if clean_text:
            list_statistics.append(TranscriptionMetricsTools(ground_truth_source.plain_text,
                                                             prediction,
                                                             image,
                                                             clean_text)
                                   )
        else:
            list_statistics.append(TranscriptionMetricsTools(ground_truth_source.plain_text,
                                                             prediction,
                                                             image)
                                   )

    report_log(f"{'#' * 10} Metrics objects created {'#' * 10}\n", "S")

    # ---- RUN 4 : Edit report sequence start
    generate_html_report(metadata, model_name, list_statistics, images)
