#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""KRAKEN-BENCHMARK CLASSES

Author : Alix Chagué
Date : 13/01/2021

Set of add-on functions to manage i/o in kraken-benchmark
"""

# built-in packages
from copy import deepcopy
import sys
import os

# external packages
from PIL import Image
from tqdm import tqdm
from kraken import lib

# local packages
from ..kb_classes.parser import (TxtParser, PagexmlParser, ImgParser)
from ..kb_utils.kb_utils import (report_log, try_control_step)


IMAGE_FORMATS = ["png", "jpg", "jpeg", "tif", "gif", "jp2"]


def collect_input(path: str, input_format: str):
    """recover the different user's files in list

        Args:
            path (str): relative path to access on user's files
            input_format (str): format of the ground truth (xml or txt)

        Returns:
            model_name (str) : basename of model
            model_file (str) : path to model
            image_files (list) : list contains user's images
            transcription_gt_files (list) : list contains user's
                                        ground truth files

    """
    def basenames(list_of_paths:list) -> list:
        """Take a list of paths and return a list of basenames."""
        return [os.path.basename(f) for f in list_of_paths]

    def collect_models(path: str) -> list:
        """Load models/*.mlmodel files in input directory"""
        models = list_directory(os.path.join(path, "models"))
        collected_models = [model for model in tqdm(models, desc="Collecting model(s)")
                            if model.endswith("mlmodel")]
        # this could be changed if necessary
        report_log(f"\n{'#' * 10} Model(s) collected \u2713 {'#' * 10}\n", "S")
        return collected_models

    def collect_text_ground_truth(path: str) -> tuple:
        """Load data/*.xml or data/*.txt files in input directory"""
        ground_truth = list_directory(os.path.join(path, "data"))
        text_ground_truth = [file for file in ground_truth if file.endswith(f".{input_format}")]
        the_rest = [file for file in ground_truth if not file.endswith(f".{input_format}")]
        report_log(f"\n{'#' * 10} Ground Truth collected \u2713 {'#' * 10}\n", "S")
        return text_ground_truth, the_rest

    def collect_images(path: str, gts: list, the_rest: list) -> list:
        """Load image files in data/"""
        images = []
        for gt in gts:
            for file in the_rest:
                if gt.replace(f".{input_format}", "") in file:
                    images.append(file)
        # control that it's a real image file
        filtered_images = [f for f in images if f.split(".")[-1] in IMAGE_FORMATS]
        report_log(f"\n{'#' * 10} Images collected \u2713 {'#' * 10}\n", "S")
        return filtered_images

    def control_pairs(imgs: list, gts: list) -> tuple:
        """Control that two lists contain the same number of
         elements eligible for pairing"""
        if len(gts) > len(imgs):
            text_files = []
            for img in imgs:
                for gt in gts:
                    if gt.replace(f".{input_format}", "") in img:
                        text_files.append(text_files)
        if len(imgs) > len(gts):
            # then it probably means that there are several image files for one given transcription
            # which means there are probably duplicated with different formats
            pass
        return imgs, gts

    path = os.path.abspath(path)
    listing_input = list_directory(path)
    if not "models" in basenames(listing_input) or not "data" in basenames(listing_input):
        report_log("Input is not correct, 2 directories are compulsory: models and data", "E")
    else:
        models = collect_models(path)
        text_ground_truth, the_rest = collect_text_ground_truth(path)
        images = collect_images(path, text_ground_truth, the_rest)
        images, text_ground_truth = control_pairs(images, text_ground_truth)

        # After these two controls, there should be exactly as many txt files as image files
        if len(models) == 0 or len(images) == 0 or len(text_ground_truth) == 0:
            report_log("Input is not correct, there is no model to test or not ground truth", "E")
        else:
            model_name = os.path.basename(models[0])  # we load only one model for now  # TODO load several models
            model_file = models[0]
            return model_name, model_file, images, text_ground_truth


def parse_ground_truth_files(files:list, format:str) -> list:
    """Generate TxtParser or PagexmlParser objects"""
    list_of_texts = []
    if format == "txt":
        for file in tqdm(files, desc="Parsing Ground Truth (txt)"):
            list_of_texts.append(TxtParser(file))
    elif format == "xml":
        for file in tqdm(files, desc="Parsing Ground Truth (xml)"):
            list_of_texts.append(PagexmlParser(file))
    report_log(f"\n{'#' * 10} Ground Truth parsed \u2713 {'#' * 10}\n", "S")
    return list_of_texts


def parse_image(image_files: list) -> list:
    """Generate ImgParser objects"""
    list_of_images = []
    for image in image_files:
        list_of_images.append(ImgParser(image))
    return list_of_images


def list_directory(dirpath):
    """Get the list of files and directories contained in a given directory excluding .DS_Store files

    :param dirpath: path to a directory
    :type dirpath: str
    :return: list of absolute paths | None if not a directory
    :retype: list
    """
    files = []
    if os.path.isdir(dirpath):
        files = [os.path.join(os.path.abspath(dirpath), f) for f in os.listdir(dirpath) if f != ".DS_Store"]
    else:
        report_log(f"{dirpath} is not a directory", "E")
    return files


def load_images(images: list, verbosity: bool) -> list:
    """Load images with PIL and return a list of loaded images (PIL objects)"""
    # At each new step we create an empty list to receive the new objects, such as :
    list_image_pil = []
    pbar = tqdm(images)
    for img in pbar:
        pbar.set_description(f"Processing '{img.split(os.sep)[-1]}' element :")
        try:
            list_image_pil.append(ImgParser(img))
        except Exception as exception:
            report_log(f"type : {exception}")
            report_log(f"Error : unable to load images - {img}", "E")
            sys.exit('program exit')
    if verbosity:
        report_log(list_image_pil, "V")
    #Do we still have the same number of items in returning list?
    try_control_step(list_image_pil, images, "Loading Images")  # TODO refactor
    return list_image_pil


def load_model(model: str) -> object:
    """Load an mlmodel with Kraken"""
    # STEP 1 : Loading model
    # Loading the model like kraken.lib.models.TorchSeqRecognizer object
    # --- Issue : find a way to ignore the output precautionary message
    model_loaded = lib.models.load_any(model)
    # At each new step, a validation message is displayed :
    report_log(f"\n{'#' * 10} Model(s) loaded \u2713 {'#' * 10}\n", "S")
    return model_loaded