#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""KRAKEN-BENCHMARK UTILS SET

Author : Lucas Terriel
Date : 22/07/2020

set of add-on functions for the kraken-benchmark.py script
"""

# built-in packages
from copy import deepcopy
import sys
import os
import shutil

# external packages
#from bs4 import BeautifulSoup
from kraken import rpred, pageseg, binarization
from prompt_toolkit.shortcuts import input_dialog
from termcolor import cprint
from tqdm import tqdm


# set default format for input (txt or xml)
XML_STANDARD = ["PcGts", "alto"] #, "TEI"]


def report_log(message, type_log="I") -> None:
    """Print a log report

    Author
    ------
    Alix Chagué

    Details
    -------

    letter code to specify type of report
    ['I' > Info|'W' > Warning|'E' > Error|'S' > Success|'V' > Verbose]

    Args:
        message (str) : message to display
        type_log (str, optional) : type of message. Defaults to "I" (Info)

    """
    if type_log == "I":  # Info
        print(message)
    elif type_log == "W":  # Warning
        cprint(message, "yellow")
    elif type_log == "E":  # Error
        cprint(message, "red")
    elif type_log == "S": # Success
        cprint(message, "green")
    elif type_log == "V":
        cprint(message, "blue")  # Verbose
    else:
        # unknown color parameter, treated as "normal" text
        print(message)


def get_list_tuple(list_1: list, list_2: list, *args: list) -> list:  # TODO refactor
    """returns sort of list containing tuples made up of elements from two or three lists

        Examples
        --------
            >>> list_1 = ['a', 'b']
            >>> list_2 = [1, 2, 3]
            >>> get_list_tuple(list_1, list_2)
            [('a', 1), ('b', 2)]
            >>> list_1 = ['a', 'b']
            >>> list_2 = [1, 2, 3]
            >>> list_3 = ['name', 'surname']
            >>> get_list_tuple(list_1, list_2, list_3)
            [('a', 1, 'name'), ('b', 2, 'surname')]

        Args:
            list_1 (list): first list to associate
            list_2 (list): second list to associate
            *args (list, optional) : third list to associate

        Returns:
            list: list with tuples contains elements of lists

        """
    if args:
        list_3 = args[0]
        list_tuple = [(list_1[index],
                       list_2[index],
                       list_3[index]) for index in range(min(len(list_1),
                                                             len(list_2),
                                                             len(list_3)))]
    else:
        list_tuple = [(list_1[index],
                       list_2[index]) for index in range(min(len(list_1),
                                                             len(list_2)))]

    return list_tuple


def get_metadata(images: list) -> list:
    """open a prompt dialogue if user activate option [label]
    and retrieve all descriptions in a list
    to display on the HTML report

        Args:
            images (list): list of user's images to attach label

        Returns:
            list : list of images'description
    """
    result = []
    for image in images:
        text = input_dialog(
            title='Images metadata',
            text=f'Enter description : {image}').run()
        if text == "":
            result.append('No metadata specify')
        else:
            result.append(text)
    return result


def get_username() -> str:
    """open a prompt dialog : alert and check the username and display it on
    HTML report

    Returns:
        str : stock username to display on HTML report
    """
    username = input_dialog(
        title='Kraken Benchmark',
        text='Please type your name: ').run()
    return username


def arrange_images_in_static(images: list) -> None:
    """manage the static folder with the new user's
    images :
    1) Push images in images folder => static folder
    2) Remove and replace images in static folder
    if user start KB again

    Note
    ----
    Flask load images only from static.

    Args:
        images (list): list of user's images
    """
    # Remove older images from static file
    dir_name = "./static"
    test = os.listdir(dir_name)

    for item in test:
        if item.endswith(".jpeg"):
            os.remove(os.path.join(dir_name, item))
        if item.endswith(".jpg"):
            os.remove(os.path.join(dir_name, item))

    # Move new images to static file
    for image in images:
        shutil.copy(image, dir_name)


def try_control_step(list_1: list, list_2: list, label_step: str) -> str:  # TODO refactor
    """perform a test to compare if two lists have the same length

    Examples
    --------

        >>> list_1 = ['a', 'b']
        >>> list_2 = [1, 2, 3]
        >>> label = 'binarization'
        >>> try_control_step(list_1, list_2, label)
        *** Error binarization ***
        TypeError: The number of elements in lists are not the same

    Args:
        list_1 (list): list to compare
        list_2 (list): another list to compare
        label_step (str): label of the current process

    Returns:
        str: success message

    Raises:
        TypeError: if len(list_1) != len(list_2)

    """
    if len(list_1) != len(list_2):
        report_log(f"*** Error {label_step} ***", "E")
        raise TypeError('The number of elements in lists are not the same')
    return report_log(f"\n{'#' * 10} {label_step} done \u2713 {'#' * 10}\n", "S")


## Transcription steps (TXT)
def binarize_images(images_loaded: list, verbosity: bool) -> list:  # obsolete
    """Binarize a series of images"""
    list_img_binarized = []
    for img_pil in tqdm(images_loaded, desc='Binarization in progress :'):
        try:
            # creates binarized images
            im_bin = binarization.nlbin(img_pil)
            list_img_binarized.append(deepcopy(im_bin))
        except Exception as exception:
            report_log(f"type : {exception}")
            report_log(f"Error : unable to binarize - {img_pil}", "E")
            sys.exit('program exit')

    if verbosity:
        report_log(list_img_binarized, "V")

    # CONTROL STEP 3
    try_control_step(list_img_binarized, images_loaded, "Binarization")
    return list_img_binarized


def segment_images(images_loaded: list, verbosity: bool) -> list:
    """Perform segmentation on a series of images"""
    # STEP 4 : Segment images
    segments_all = []
    for img in tqdm(images_loaded, desc='Segmentation in progress :'):
        # retrieves the coordinates of the segments from the binarized image
        try:
            segments_image = pageseg.segment(img.binarized, text_direction='horizontal-lr')
            segments_all.append(deepcopy(segments_image))
        except Exception as exception:
            report_log(f"type : {exception}")
            report_log(f"Error : unable to segment - {img.binarized}", "E")
            sys.exit('program exit')

    if verbosity:
        report_log(segments_all, "V")

    # CONTROL STEP 4 :
    try_control_step(segments_all, images_loaded, "Segmentation")
    return segments_all


def predict_transcription(pairs_image_segments: list, model_loaded: object, verbosity: bool) -> list:
    """Perform transcription on a series of images given a series of segments"""
    list_predictions = []
    counter = 0
    while len(pairs_image_segments) != len(list_predictions):
        for pair in pairs_image_segments:
            for element in pair:
                # extract from the list binarized image-segmented image the
                # elements one by one to transform them into an
                # kraken.rpred.mm_rpred object content the prediction of the text
                # if the counter is even, get binarized image
                if counter % 2 == 0:
                    image = element
                    counter += 1
                    # if the counter is odd, get segmented image
                else:
                    segment_element = element
                    # created the text predictions (kraken.rpred.mm_rpred object)
                    output_rpred = rpred.rpred(model_loaded,
                                               image.binarized,
                                               segment_element,
                                               bidi_reordering=True)
                    list_predictions.append(output_rpred)
                    counter += 1
    if verbosity:
        report_log(list_predictions, "V")
    return list_predictions


def is_binary_model(model_loaded: object) -> bool:
    """Determine whether a model is binary or not"""
    # https://github.com/mittagessen/kraken/blob/8b0d15d7c500260904065334a63f15c9e141b503/kraken/rpred.py#L180
    # and
    # https://github.com/mittagessen/kraken/blob/8b0d15d7c500260904065334a63f15c9e141b503/kraken/rpred.py#L353
    '''
    if "nets" in model_loaded.__dict__.keys():
        nets = model_loaded.nets
    elif "network" in model_loaded.__dict__.keys():
        nets = model_loaded.network
    else:
        report_log("Can't tell if model in binary or not", "W")
        report_log(f"[DEBUG] : {model_loaded.__dict__.keys()}", "W")
    '''
    nets = {'default': model_loaded}
    one_channel_modes = set(recognizer.nn.one_channel_mode for recognizer in nets.values())
    return '1' in one_channel_modes
