#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Running Kraken Benchmark
author: Alix Chagué
date: 07/01/2021
"""

# imports
import argparse
#import os

from kblib import kraken_benchmark as kb
from kblib.kb_utils.kb_utils import (report_log)

# parse args

# ----- ARGPARSE OPTIONS FOR THE CLI PROGRAM
PARSER = argparse.ArgumentParser(description="Launch the Kraken-Benchmark CLI to \n" +
                                 "generate a HTML dashboard with Flask\n" +
                                 "to show metrics that evaluate your \n" +
                                 "(kraken) transcription model(s)")

PARSER.add_argument('--input', '-i', action='store', default='./data_test/',
                    help='specify location of images, models and ground truth\n' +
                    'if not in default location (default = ./data_test/)')

PARSER.add_argument("--format", "-f", action="store", default="txt",
                    help="specify the format of the ground truth files\n' + "
                    "(supported: txt, xml; default = txt)")

PARSER.add_argument('--verbosity', '-v', action='store_true',
                    help='activate verbosity')

PARSER.add_argument('--label', '-l', action='store_true',
                    help='attach a label to images')

PARSER.add_argument('--clean_text', '-c', action='store_true',
                    help='tokenization with clean steps applied : \n' +
                    'replace new line and carriage return with nothing and \n' +
                    'replace numbers and punctuation with space')

PARSER.add_argument('-m', '--mode', action='store', nargs=1, default='default',
                    help="default|test")
ARGS = vars(PARSER.parse_args())


# start main task
if ARGS['mode'][0].lower() == 'test':
    pass
elif ARGS['mode'].lower() == 'default':
    kb.kraken_benchmark(
        path_to_input=ARGS['input'],
        verbosity=ARGS['verbosity'],  # opt_verbose
        label=ARGS['label'],
        clean_text=ARGS['clean_text'],
        input_format=ARGS['format'].lower()
    )

else:
    report_log(f"{ARGS['mode']} is not a valid mode", "E")
